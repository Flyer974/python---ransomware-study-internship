# ------ Libraries imports ------- #
import csv
import matplotlib.pyplot as plt
from matplotlib import style

#______________OPENING THE CSV FILE_____________#
def open_csv_values():
    with open('data.csv', 'r') as infile:
        #read the file as a dictionary for each row ({header : value})
        reader = csv.DictReader(infile)
        data = {}
        for row in reader:
            for header, value in row.items():
                try:
                    data[header].append(value)
                except KeyError:
                    data[header] = [value]
        # extract the variables you want
        memory_values = data['RAM']
        cpu_values = data['CPU']
        disk_values = data['disk']
        rc = []
        wc = []
        rb = []
        wb = []
        oc = []
        ob = []
        for value in disk_values:
            split = value[2:-2]
            rc.append(int(split.split("' '")[0]))
            wc.append(int(split.split("' '")[1]))
            rb.append(int(split.split("' '")[2]))
            wb.append(int(split.split("' '")[3]))
            oc.append(int(split.split("' '")[4]))
            ob.append(int(split.split("' '")[5]))
    return memory_values, cpu_values, rc, wc, rb, wb, oc, ob

#----Converting a list in float----#
def GraphPlot():
    res = open_csv_values()
    memory_values = res[0]
    cpu_values = res[1]

    #Converting in float
    cpu_float_list = []
    for item in cpu_values:
        cpu_float_list.append(float(item))

    memory_float_list = []
    for item in memory_values:
        memory_float_list.append(float(item))

    # ------ Graph Style used------ #
    style.use('fivethirtyeight')

    # ------ Plotting the Graph ------ #
    fig = plt.figure(1)

    # ------ Plotting other graph for the CPU, MEMORY & DISK USAGE ------ #
    ax1 = fig.add_subplot(111)
    ax2 = fig.add_subplot(111)

    # ------ Putting Label for the graph ------ #
    plt.ylabel('Usage in %')
    plt.xlabel('Time in second')

    # ------ Plotting values in axis ------ #
    ax1.plot(cpu_float_list)
    ax2.plot(memory_float_list)

    # ------ Labels used for axis ------ #
    ax1.legend(['CPU USAGE', 'MEMORY USAGE'])

    #___________________PLOTING READ AND WRITE COUNT_______________#
    res = open_csv_values()
    rc = res[2]
    wc = res[3]

    #Converting in float
    rc_float = []
    for item in rc:
        rc_float.append(float(item))

    wc_float = []
    for item in wc:
        wc_float.append(float(item))

    # ------ Plotting the Graph ------ #
    fig = plt.figure(2)

    # ------ Plotting other graph for the CPU, MEMORY & DISK USAGE ------ #
    ax1 = fig.add_subplot(111)
    ax2 = fig.add_subplot(111)

    # ------ Putting Label for the graph ------ #
    plt.ylabel('Read')
    plt.xlabel('Time in second')

    # ------ Plotting values in axis ------ #
    ax1.plot(rc_float)
    ax2.plot(wc_float)

    # ------ Labels used for axis ------ #
    ax1.legend(['READ COUNT', 'WRITE COUNT'])

    #___________________PLOTING READ AND WRITE BYTES_______________#
    rb = res[4]
    wb = res[5]

    rb_float = []
    for item in rb:
        rb_float.append(float(item))

    wb_float = []
    for item in wb:
        wb_float.append(float(item))

    # ------ Plotting the Graph ------ #
    fig = plt.figure(3)

    ax1 = fig.add_subplot(111)
    ax2 = fig.add_subplot(111)

    # ------ Putting Label for the graph ------ #
    plt.ylabel('Read')
    plt.xlabel('Time in second')

    # ------ Plotting values in axis ------ #
    ax1.plot(rb_float)
    ax2.plot(wb_float)

    # ------ Labels used for axis ------ #
    ax1.legend(['READ BYTES', 'WRITE BYTES'])

    #___________________PLOTING OTHERS READ AND WRITE COUNT______________#
    oc = res[6]
    ob = res[7]

    oc_float = []
    for item in oc:
        oc_float.append(float(item))

    ob_float = []
    for item in ob:
        ob_float.append(float(item))

    # ------ Plotting the Graph ------ #
    fig = plt.figure(4)
    ax1 = fig.add_subplot(111)
    ax2 = fig.add_subplot(111)

    # ------ Putting Label for the graph ------ #
    plt.ylabel('Read')
    plt.xlabel('Time in second')

    # ------ Plotting values in axis ------ #
    ax1.plot(oc_float)
    ax2.plot(ob_float)

    # ------ Labels used for axis ------ #
    ax1.legend(['OTHER READ', 'OTHER COUNT'])

    #-----Ploting the graph------#
    plt.show()

print(GraphPlot())