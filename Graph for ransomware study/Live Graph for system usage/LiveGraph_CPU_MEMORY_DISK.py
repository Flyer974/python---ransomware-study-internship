# ------ Libraries imports ------- #
import psutil
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style

# ------ Function in order to animate our graph
def animate(i):

    # ------ Adding the new values in the list ------ #
    ys_cpu.append(psutil.cpu_percent(None, False))
    ys_memory.append(psutil.virtual_memory().percent)
    ys_diskusage.append(psutil.disk_usage('/').percent)
    ys_swapmemory.append(psutil.virtual_memory().percent)

    # ------ Clearing the axis ------ #
    ax1.clear()

    # ------ Putting Label for the graph ------ #
    plt.ylabel('Usage in %')
    plt.xlabel('Time in second')

    # ------ Plotting values in axis ------ #
    ax1.plot(ys_cpu)
    ax2.plot(ys_memory)
    ax3.plot(ys_diskusage)
    ax4.plot(ys_swapmemory)

    # ------ Axis limits ------ #
    ax1.set_xlim([0, 150])
    ax1.set_ylim([0, 100])

    # ------ Labels used for axis ------ #
    ax1.legend(['CPU USAGE', 'MEMORY USAGE', 'DISK USAGE', 'SWAP MEMORY'])

# ------ Graph Style used ------ #
style.use('fivethirtyeight')

# ------ Plotting the Graph ------ #
fig = plt.figure()

# ------ Plotting other graph for the CPU, MEMORY & DISK USAGE ------ #
ax1 = fig.add_subplot(111)
ax2 = fig.add_subplot(111)
ax3 = fig.add_subplot(111)
ax4 = fig.add_subplot(111)

# ------ We initialize three lists in order to stock values
# ~ List for the cpu usage ~ #
ys_cpu = []
# ~ List for the memory usage ~ #
ys_memory = []
# ~ List for the disk usage ~ #
ys_diskusage = []
# ~ List for the swap memory usage ~ #
ys_swapmemory = []

# ------ Setup the animation with a 200 milliseconds interval ------ #
ani = animation.FuncAnimation(fig, animate, interval=200)

# ------ Plotting the graph ------ #
plt.show()